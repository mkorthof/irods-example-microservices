# irods-example-microservices 

CMake "Template" project for [irods-microservices](https://gitlab.com/mkorthof/irods-microservices)

Directory structure:

- irods-example-microservices/CMakeModules
- irods-example-microservices/CMakeLists.txt
- irods-example-microservices/package/description.txt
- irods-example-microservices/src/irods_includes.hh
- irods-example-microservices/src/msiExample.cc
- irods-example-microservices/tests

Used CMakeModules:

- irods-example-microservices/CMakeModules/DetectOS.cmake
- irods-example-microservices/CMakeModules/FindJansson.cmake

Also see 'CMake' from [README.md](https://gitlab.com/mkorthof/irods-microservices/-/blob/main/README.md#cmake) in main repo.

## Usage 

This repository is meant to be cloned and used with your own GitLab. The CI part works on both self-managed and GitLab SaaS.

1) Replace 'msiExample.cc' with your own msi source code file(s) and modify 'CMakeLists.txt' accordingly:

    - name in `project()` (top)
    - irods version in `find_package(IRODS ...)`
    - msi's in `add_library()`, `target_link_libraries()` and `install()`
    - multiple settings in `CPACK_PACKAGE*` (bottom)

2) Add irule(s) to 'tests' dir: 'irods-example-microservices/tests/helloworld.r'

3) Add (RPM) package description: 'package/description.txt'.

## Build

Run these commands to build msi libs:

``` shell
# Make sure cmake is in PATH first (version may differ)
PATH=$PATH:/opt/irods-externals/cmake3.11.4-0/bin

cmake . && make

# Cleanup
make clean
rm -rf cmake _CPack_Packages CMakeFiles CMakeCache.txt install_manifest.txt *.cmake libmsi*.so *.rpm

# Install *.so files to /usr/lib/irods/plugins/microservices
make install

# Create package
clean && cmake . && make package
```
