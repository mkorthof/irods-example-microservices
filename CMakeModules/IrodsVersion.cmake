set(Python_ADDITIONAL_VERSIONS "3")
find_package(PythonInterp)
if(NOT DEFINED PYTHON_EXECUTABLE)
     set(PYTHON_EXECUTABLE "python")
endif()
exec_program("${PYTHON_EXECUTABLE} \
             -c \"import json, os; \
                ver_path = '/var/lib/irods/VERSION.json' if os.path.exists('/var/lib/irods/VERSION.json') else '/var/lib/irods/version.json'; \
                print(str(json.load(open(ver_path)).get('irods_version', '')))\""
             OUTPUT_VARIABLE IRODS_VERSION)
